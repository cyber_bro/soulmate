// Copyright Epic Games, Inc. All Rights Reserved.

#include "SOULMATE.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, SOULMATE, "SOULMATE");

DEFINE_LOG_CATEGORY(LogSOULMATE)
