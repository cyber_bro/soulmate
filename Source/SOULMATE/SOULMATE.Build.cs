// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SOULMATE : ModuleRules
{
	public SOULMATE(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PrivateIncludePaths.Add( "SOULMATE/" );

		PublicDependencyModuleNames.AddRange(new string[]
			{"Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "DirectoryWatcher"});
	}
}