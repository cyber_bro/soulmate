// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SOULMATEGameMode.generated.h"

UCLASS(minimalapi)
class ASOULMATEGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASOULMATEGameMode();
};



