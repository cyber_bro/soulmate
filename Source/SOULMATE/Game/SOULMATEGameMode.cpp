// Copyright Epic Games, Inc. All Rights Reserved.

#include "SOULMATEGameMode.h"
#include "SOULMATEPlayerController.h"
#include "Character/SOULMATECharacter.h"
#include "UObject/ConstructorHelpers.h"

ASOULMATEGameMode::ASOULMATEGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASOULMATEPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(
		TEXT("/Game/Blueprints/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
